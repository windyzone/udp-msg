package cn.xyt.service;

import org.springframework.stereotype.Service;

import cn.tom.kit.IoBuffer;
import cn.xyt.dto.Message;
import cn.xyt.util.BCDUtil;
import cn.xyt.util.JPushUtil;

@Service
public class MessageService extends BaseService{
	
	
	public byte[] post_A8(Message msg) { // 上传数据包
		IoBuffer body = msg.getBodyBuf();
		byte[] header = body.readBytes(4);
		int port = header[1]&0xff;
		int len = body.readByte()&0xff;
		//TODO 处理 数据
		
		//AA 01 00 A8 15 
		//00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 16卡号
		//00 01
		//00 01 2C  //消费金额
		byte [] card = body.readBytes(16);
		byte[] serial = body.readBytes(2);
		body.readByte();
		int money = body.readShort();
		
		IoBuffer resp = IoBuffer.allocate(header.length+6+2);  //len +2 长度注意
		resp.writeBytes(header)
			.writeByte(6); //len +2
		boolean flag = true;
		if(flag){
			resp.writeByte(1);
			resp.writeBytes(serial);
			resp.writeByte(1);//1000
			resp.writeShort((short)0); //1000
			resp.writeByte(0);
		}else{
			resp.writeByte(0);
			resp.writeBytes(serial);
			resp.writeInt(0);
		}
		return resp.array();
	}
	
	public byte[] post_B6(Message msg) { // 上传数据包
		IoBuffer body = msg.getBodyBuf();
		byte[] header = body.readBytes(4);
		int port = header[1]&0xff;
		int len = body.readByte()&0xff;
		//TODO 处理 数据
		
		IoBuffer resp = IoBuffer.allocate(header.length+3);
		resp.writeBytes(header)
			.writeByte(1).writeByte(1).writeByte(1);
		return resp.array();
	}
	
	
	/**
	 * 刷卡 网络卡消费结果上报
	 * @param msg
	 * @return
	 */
	public byte[] post_C8(Message msg) {
		IoBuffer body = msg.getBodyBuf();
		byte[] header = body.readBytes(4);
		int port = header[1]&0xff;
		int len = body.readByte()&0xff;
		//TODO 处理 数据
		//卡号(16byte)+订单序号(2byte)+消费金额(3byte)+工作时长
		//[分钟](2byte)+订单完成时间(4byte)
		body.readBytes(12);
		byte[] card = body.readBytes(4);
		String card0 = BCDUtil.bcd2String(card);
		
		body.readShort();
		
		byte [] money = body.readBytes(3);
		IoBuffer d = IoBuffer.allocate(4);
		d.writeByte(0).writeBytes(money);
		int money0 = d.flip().readInt();
		int keep = body.readShort();
		JPushUtil.pushOrderStat(card0, 0, money0, keep);
		
		IoBuffer resp = IoBuffer.allocate(header.length+3);
		resp.writeBytes(header)
			.writeByte(1).writeByte(1).writeByte(1);
		return resp.array();
	}
	
	
	/**
	 * 消费结果上报
	 * @param msg
	 * @return
	 */
	public byte[] post_C9(Message msg) {
		IoBuffer body = msg.getBodyBuf();
		byte[] header = body.readBytes(4);
		int port = header[1]&0xff;
		int len = body.readByte()&0xff;
		//TODO 处理 数据
		//订单号(8byte)+订单序号(2byte)+消费金额(3byte)+工作时长
		//[分钟](2byte)+订单完成时间(4byte)
		long serial = body.readLong();
		body.readShort();
		
		byte [] money = body.readBytes(3);
		IoBuffer d = IoBuffer.allocate(4);
		d.writeByte(0).writeBytes(money);
		int money0 = d.flip().readInt();
		int keep = body.readShort();
		JPushUtil.pushOrderStat("",serial, money0, keep);
		
		IoBuffer resp = IoBuffer.allocate(header.length+3);
		resp.writeBytes(header)
			.writeByte(1).writeByte(1).writeByte(1);
		return resp.array();
	}
	
	/**
	 * 设备状态
	 * @param msg
	 * @return
	 */
	public byte[] post_C4(Message msg) {  
		IoBuffer body = msg.getBodyBuf();
		byte[] header = body.readBytes(4);
		int port = header[1]&0xff;
		int len = body.readByte()&0xff;
		//TODO 处理 数据
		
		IoBuffer resp = IoBuffer.allocate(header.length+3);
		resp.writeBytes(header)
		.writeByte(1).writeByte(1).writeByte(1);
		return resp.array();
	}


}
