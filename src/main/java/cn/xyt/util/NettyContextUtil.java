package cn.xyt.util;

import io.netty.channel.ChannelHandlerContext;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;

public class NettyContextUtil {
	public static void setAttr(ChannelHandlerContext ctx, String key, Object obj) {
		AttributeKey<Object> attrKey = AttributeKey.valueOf(key);
		Attribute<Object> attr = ctx.channel().attr(attrKey);
		attr.set(obj);
	}

	public static Object getAttr(ChannelHandlerContext ctx, String key) {
		AttributeKey<Object> attrKey = AttributeKey.valueOf(key);
		Attribute<Object> attr = ctx.channel().attr(attrKey);
		return attr.get();
	}
}
