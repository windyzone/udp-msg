package cn.xyt.util;

import cn.tom.protocol.http.HttpMessage;
import cn.tom.transport.Session;


public class PullSession { 
	Session session;
    HttpMessage pullMessage;  
   
	public PullSession(Session sess, HttpMessage pullMessage) { 
		this.session = sess;
		this.setPullMessage(pullMessage);
	}  
	public Session getSession() {
		return session;
	}
	
	public HttpMessage getPullMessage() {
		return this.pullMessage;
	}
	
	public void setPullMessage(HttpMessage msg) { 
		synchronized (session) {
			this.pullMessage = msg;
		}
	}  

}

