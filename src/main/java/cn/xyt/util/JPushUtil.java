package cn.xyt.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.client.AsyncClientHttpRequest;

import cn.tom.kit.StringUtil;
import cn.tom.protocol.http.HttpMessage;
import sun.misc.BASE64Encoder;

/**
 * 	推送 消息分三块
	1. 电池信息, 充电, 电量
	2. 震动报警
 * 	@author tomsun	
 */
public class JPushUtil {
	private static final String appkey = "e72496fa54e5adfed5d65884";
	private static final String masterSecret = "ba352a518f014cf8351c8bcc";
	private static final String auth =new  BASE64Encoder().encode((appkey+":"+masterSecret).getBytes());//base64(appKey:masterSecret)
	
	/**
	 * 推送所有设备
	 * @param alert
	 * @return
	 * @throws Exception
	 */
	public static String pushAll(String alert) throws Exception {
		AsyncClientHttpRequest request = HttpUtil.createRequest("https://api.jpush.cn/v3/push", HttpMethod.POST);
		request.getHeaders().set("Authorization", "Basic " + auth);
		request.getHeaders().set("Content-Type", "application/json");
		//'options':{'apns_production':false},  //TODO 去掉修改正式环境, 或改true
		byte content[] = ("{'platform':'all','audience':'all','notification':{'alert':'" + alert + "'}}").replace('\'', '"').getBytes("UTF-8");
		request.getBody().write(content);
		return HttpUtil.exec(request);
	}
	
	/**
	 * 推送指定注册设备
	 * @param alert
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public static String pushByid(String alert,String sound, String[] id ) throws Exception{
		AsyncClientHttpRequest request = HttpUtil.createRequest("https://api.jpush.cn/v3/push", HttpMethod.POST);
		request.getHeaders().set("Authorization", "Basic " + auth);
		request.getHeaders().set("Content-Type", "application/json");
		String ids = StringUtil.join(id, ",");
		sound = StringUtil.isEmpty(sound) ? "default" : sound;
		//'options':{'apns_production':false}, 'ios' : {'sound': 'default'}  //TODO 去掉修改正式环境, 或改true IOS需要加默认推送音 
		String pcontent ="{'options':{'apns_production':true},'platform':'all','audience':{'alias':["+ids+"]},"
						+ "'message':{'msg_content':'"+alert+"','extras': {'sound': '"+sound+"'}}," //自定义消息
						+ "'notification':{'ios' : {'alert':'"+alert+"', 'sound': '"+sound+"'}}}"; //通知,IOS 专用
		byte content [] = pcontent.replace('\'', '"').getBytes("UTF-8");
		request.getBody().write(content);
		HttpUtil.logger.info(request.getURI()+"::"+new String(content));
		return HttpUtil.exec(request);
	}
	

	
	
	public static Map<String, PullSession>  pullMap = new HashMap<>();
	
	
	
	/**
	 * 
	 * @param deviceid
	 * @param boolean start = "1".equals(msg.getParam("start"));
	 * 1 启动 0 关闭
	 * 推送启动关闭标记, 记录车辆 分段轨迹
	 */
	public static void pushOrderStat(String card,long serial, int money, int keep){
		PullSession session = pullMap.get("pushMsg"); //topic
		if(session == null) return;
		HttpMessage msg = new HttpMessage();
		msg.setCommand("pushMsg");
		msg.setHead("type", "orderStat");
		msg.setHead("serial", ""+serial);
		msg.setHead("card", ""+card);
		msg.setHead("money", ""+money);
		msg.setHead("keep", ""+keep);
		msg.setBody(new byte[]{});
		session.getSession().write(msg);
	}
	
	
	
	public static void main(String[] args) throws Exception {
//		System.out.println(pushAll("helloall"));;
//		System.out.println(pushByid("hello by 5555", "'5'"));
//		System.out.println(pushByid("hello by 666666", "'6'"));
		
	}

}
