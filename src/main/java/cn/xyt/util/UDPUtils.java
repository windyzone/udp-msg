package cn.xyt.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JT808协议转义工具类
 * 
 * <pre>
 * 0x8D AD <====> 0x8D
 * 0x8D AE <====> 0x8E
 * 0x8D AF <====> 0x8F
 * </pre>
 * 
 * @author hylexus
 *
 */
public class UDPUtils {
	
	public static void main(String[] args) {
		int a =  0x8F^0x20;
		System.out.println(Integer.toHexString(a));
	}

	/**
	 * 接收消息时转义<br>
	 * <pre>
	 *0x8D AD <====> 0x8D
	 * 0x8D AE <====> 0x8E
	 * 0x8D AF <====> 0x8F
	 * </pre>
	 * @param bs
	 *            要转义的字节数组
	 * @param start
	 *            起始索引
	 * @param end
	 *            结束索引
	 * @return 转义后的字节数组
	 * @throws Exception
	 */
	public static byte[] doEscape4Receive(byte[] bs, int start, int end) {
		if (start < 0 || end > bs.length)
			throw new ArrayIndexOutOfBoundsException("doEscape4Receive error : index out of bounds(start=" + start
					+ ",end=" + end + ",bytes length=" + bs.length + ")");
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			for (int i = 0; i < start; i++) {
				baos.write(bs[i]);
			}
			for (int i = start; i < end - 1; i++) {
				if (bs[i] == 0x8D && bs[i + 1] == 0xAD) {
					baos.write(0x8D);
					i++;
				} else if (bs[i] == 0x8d && bs[i + 1] == 0xAE) {
					baos.write(0x8E);
					i++;
				}  else if (bs[i] == 0x8d && bs[i + 1] == 0xAF) {
					baos.write(0x8F);
					i++;
				} else {
					baos.write(bs[i]);
				}
			}
			for (int i = end - 1; i < bs.length; i++) {
				baos.write(bs[i]);
			}
			return baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				baos = null;
			}
		}
		return bs;
	}

	/**
	 * 
	 * 发送消息时转义<br>
	 * 
	 * <pre>
	 *  *0x8D AD <====> 0x8D
	 *  <====> 0x8E  --> 0x8D AE
	 *  <====> 0x8F  --> 0x8D AF
	 * </pre>
	 * 
	 * @param bs
	 *            要转义的字节数组
	 * @param start
	 *            起始索引
	 * @param end
	 *            结束索引
	 * @return 转义后的字节数组
	 * @throws Exception
	 */
	public static byte[] doEscape4Send(byte[] bs, int start, int end) {
		if (start < 0 || end > bs.length)
			throw new ArrayIndexOutOfBoundsException("doEscape4Send error : index out of bounds(start=" + start
					+ ",end=" + end + ",bytes length=" + bs.length + ")");
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			for (int i = 0; i < start; i++) {
				baos.write(bs[i]);
			}
			for (int i = start; i < end; i++) {
				if (bs[i] == 0x8e) {
					baos.write(0x8d);
					baos.write(0xAE);
				} else if (bs[i] == 0x8F){
					baos.write(0x8d);
					baos.write(0xAF);
				}else {
					baos.write(bs[i]);
				}
			}
			for (int i = end; i < bs.length; i++) {
				baos.write(bs[i]);
			}
			return baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				baos = null;
			}
		}
		return bs;
	}

}
