package cn.xyt.dto;

import cn.tom.kit.IoBuffer;

/**
 * 	
	//8E 88
	//50 44 05 A6 C4
	//00 AB 00 09   // 
	//AA 00 00 A1 01 01 0B  // 透传内容
	//59
	//8F
 */
public class Header {   
	
	public byte[] toByteBuf() {
		IoBuffer buf = IoBuffer.allocate(9);
		buf.writeBytes(this.getDeviceid());
		buf.writeShort(this.getMsgid());
		buf.writeShort(this.getLength());
		
		return buf.array() ;
	}
	
	public long getDeviceidByByte(){
		String d = "1";
		for (int i = 0; i < deviceid.length; i++) {
			if(i==3){
				int a = (int)(deviceid[i] & 0x7f);
				d += (a>10? a: "0"+a );
			}else{
				int a =  (int)deviceid[i];
				d += (a>10? a: "0"+a )  ;
			}
		}
		return  Long.parseLong(d); 
	}

	private byte[] deviceid;
	private short msgid = 0xAB; 
	private short length;
	
	public short getMsgid() {
		return msgid;
	}

	public void setMsgid(short msgid) {
		this.msgid = msgid;
	}

	public short getLength() {
		return length;
	}

	public void setLength(short length) {
		this.length = length;
	}
	
	public byte[] getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(byte[] deviceid) {
		this.deviceid = deviceid;
	}


	public static void main(String[] args) {
		int a= 8;
		
		System.out.println(String.format("%02X ", a));
	}
	
	
	
}
