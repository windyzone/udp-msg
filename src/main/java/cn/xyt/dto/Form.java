package cn.xyt.dto;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Form {

	private String code;

	private String deviceid;

	private String cmd;

	private String resp_cmd;
	
	private CountDownLatch cdl = new CountDownLatch(1);
	
	private Map<String,Object> resp = error("发送命令失败");

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getResp_cmd() {
		return resp_cmd;
	}

	public void setResp_cmd(String resp_cmd) {
		this.resp_cmd = resp_cmd;
	}

	public CountDownLatch getCdl() {
		return cdl;
	}

	public void release() {
		cdl.countDown();
	}

	public Map<String, Object> getResp() {
		return resp;
	}

	public void setResp(Map<String, Object> resp) {
		this.resp = resp;
	}
	
	
	public Map<String, Object> error(String msg, Object... obj){
		HashMap<String , Object> error = new HashMap<>();
		error.put("msg", msg);
		error.put("state", "001");
		if(obj!=null && obj.length>0){
			error.put("data", obj[0]);
		}
		return error;
	}
	
	public static String toByteArray(byte[] bb){
		StringBuilder builder = new StringBuilder(bb.length);
		for (byte byteChar : bb){
			builder.append(String.format("%02X ", byteChar));
		}
		return builder.toString();
	}

}
