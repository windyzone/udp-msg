package cn.xyt.dto;

import cn.tom.kit.IoBuffer;
import cn.xyt.util.ByteUtil;

public class Message {
	
	//8E 88
	//50 44 05 A6 C4
	//00 AB 00 09   // 
	//AA 00 00 A1 01 01 0B  // 透传内容 
	
	//59 /*总校验位*/
	//8F
	
	/*消息头*/
	private Header header;
	/*消息体*/
	//AA 00 00 A1 01 01 0B  // 透传内容 
	private byte[] body; //01
	
	/*总校验位*/
	private byte check;
	
	public Message() {
	}
	
	public Message(Header header) {
		this.header = header;
	}

	public byte getCheck() {
		return check;
	}
	
	/**
	 * check 位 根据 header + body 的 byte[] 数组1,length 异或 byte[0]^byte[1]...byte[length]
	 * @return
	 */
	public void intCheck(){
		byte[] header = getHeader().toByteBuf();
		byte[] bb = header;
		if(body != null){
			IoBuffer buf = IoBuffer.allocate(header.length +body.length );
			buf.writeBytes(header);
			buf.writeBytes(body);
			bb = buf.array();
		}
		byte check = (byte)0;
		for (int i = 0; i < bb.length; i++) {
			check  = (byte) (check ^ bb[i]);
		}
		this.check = (byte) (check & 0xFF);
	}
	
	public void setCheck(byte check) {
		this.check = check;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}
	
	public void setBodyCheck(){
		byte check = (byte)0;
		int len =body.length;
		for (int i = 0; i < len-1; i++) {
			check  = (byte) (check ^ body[i]);
		}
		body[len-1] =check;  //0123
	}

	public IoBuffer getBodyBuf() {
		IoBuffer buf = IoBuffer.wrap(body);
		return buf;
	}
	
	@Override
	public String toString() {
		return "[devicei="+getHeader().getDeviceidByByte()+", msgid = " +Integer.toHexString(body[3]&0xff) +"]";
	}
	
	
}
