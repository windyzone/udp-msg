package cn.xyt.net;

import java.io.IOException;

import cn.tom.protocol.http.HttpMessage;
import cn.tom.protocol.http.HttpMessageAdaptor;
import cn.tom.transport.nio.NioClient;

/**
 * 测试 CmdController 的类
 * @author tomsun
 *
 */
public class Client {
	public static void main(String[] args) throws IOException, InterruptedException {

		NioClient<HttpMessage> client = new NioClient("121.42.247.56:7070");  //121.42.247.56
		client.setIoAdaptor(new HttpMessageAdaptor());
		HttpMessage msg = new HttpMessage();
		msg.setCommand("get");
		msg.getMeta().setParam("module", "execsql");
		msg.getMeta().setParam("sql", "update deviceset set volt =?, ah=?, max_speed=? where deviceid in (2772,2774)");
		msg.setBody("[40, 20, 40]");
		client.invokeSync(msg);
//		msg.getMeta().setParam("cmds", "11");
//		msg.getMeta().setParam("code", "1,1"); //1,1  2,1
//		msg.getMeta().setParam("deviceid", "13");
//		msg.setHead("cmds", "11");
//		msg.setHead("code", "1,1");
//		msg.setHead("deviceid", "123");
		
		long a = System.currentTimeMillis();
//		for(int i=0;i<10;i++){
			msg = client.invokeSync(msg);
//		}
		System.out.println(System.currentTimeMillis() -a);
		System.out.println("======" +msg);
		
		
//		client.invokeAsync(msg,new ResultCallback() {
//			
//			@Override
//			public void onCompleted(Id result) {
//				System.out.println("============="+result);
//			}
//		});
		
		
//		client.close();   // 关闭通道
	
	}

}
