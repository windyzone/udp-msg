package cn.xyt.server;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tom.db.jdbc.simple.DBUtil;
import cn.tom.kit.io.PropertiesUtil;
import cn.tom.kit.io.Resource;
import cn.tom.mvc.config.Constants;
import cn.xyt.dto.SystemProperty;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

@Service
public class ServerBootStart {
	private static final Logger LOG = LoggerFactory.getLogger(ServerBootStart.class);
	@Autowired
	private SystemProperty property;
	@Autowired
	MessageCoder messageCoder;
	
	private int workthreads = Runtime.getRuntime().availableProcessors();
	private int bossthreads = workthreads/2;
	
	void initServer()throws InterruptedException {
		EventLoopGroup boss = new NioEventLoopGroup(bossthreads);
		try{
		Bootstrap bootstrap = new Bootstrap();// 启动辅助类
		bootstrap.channel(NioDatagramChannel.class)
			.group(boss)
			.option(ChannelOption.SO_BROADCAST, true)
			.handler(new ChannelInitializer<NioDatagramChannel>() {
				@Override
				protected void initChannel(NioDatagramChannel socketChannel) throws Exception {
					int deviceHeartTime = ServerBootStart.this.property.getDeviceHeart().intValue();
					ChannelPipeline p = socketChannel.pipeline();
					p.addLast("logging", new LoggingHandler(LogLevel.INFO)); // 添加byte 数据日志, 用于调试
					//读, 写, 总超时
//					p.addLast("idleStateHandler", new IdleStateHandler(0, 0, deviceHeartTime)); 
//					p.addLast("heartHandler", new HeartHandler());
					// UDP没有粘包拆包, 直接一个包 收到就收到,超出2K可能太大收不到
//					p.addLast("limiter", new DelimiterBasedFrameDecoder(100, Unpooled.copiedBuffer(new byte[] { (byte) 0x8F }))); // in 顺序
					p.addLast("messageInDecoder", messageCoder);
//					p.addLast("messageInDecoder", new ServerHandler());
					/*p.addLast("encoder1", new Encoder()); // out 逆序*/
					
					/* 执行顺序BaseInDecode ->MessageDecoder  ->messageEncoder ->[out]encoder1 -->  BaseOutEncoder ->*/
					
				}
			});
			
			ChannelFuture f = bootstrap.bind(this.property.getMessageServerPort().intValue()).sync();
			if (f.isSuccess()) {
				LOG.info("*****************MessageServer start success******************************");
			} else {
				LOG.info("*****************MessageServer start faild******************************");
			}
			 f.channel().closeFuture().await();
		}finally{
            boss.shutdownGracefully();
		}
	}
	
	@PostConstruct
	void init() throws InterruptedException, IOException {
		initDataSource();
		
		Thread th = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					initServer();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		th.setDaemon(false);
		th.start();
	}
	
	
	public void initDataSource() throws IOException{
//		new DefaultLogConfigure().config();
		Resource resource = new Resource(ServerBootStart.this.property.getDBconfig());
		Properties props = PropertiesUtil.loadProperties(resource);
		if("SQL".equals(Constants.getDBType())){
			DBUtil.init(props);
			return;
		}
	}

	@PreDestroy
	public void destroy() {
	}
}
