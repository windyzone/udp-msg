package cn.xyt.client;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * ByteToMessageDecoder extends ChannelInboundHandlerAdapter
 * 自动会做组包操作, 使用 ChannelInboundHandlerAdapter, 不会自动组包
 * 推荐使用这个
 **/
public class ByteInDecoder extends ByteToMessageDecoder{
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		/* 自动会做组包操作, 使用 ChannelInboundHandlerAdapter, 不会自动组包*/
		in.markReaderIndex();
		boolean flag = false;    //自己处理拆包逻辑
		if(flag){
			in.resetReaderIndex();  //reset 后 byteToMessageDecoder 会自动帮忙把过来的数据粘上去
			return;
		}
		Msg obj = byteToObj(in);
		out.add(obj);
	}
	
	public Msg byteToObj(ByteBuf buf){
		// 转换
		return new Msg();
	}
	
	public static class Msg{
		public byte[] content;
		public Msg(byte[] content) {
			this.content = content;
		}
		public Msg() {
		}
		
	}

}