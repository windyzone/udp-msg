package cn.xyt.client;


import static java.nio.ByteOrder.LITTLE_ENDIAN;

import java.nio.ByteBuffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.MessageToByteEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.xyt.client.ByteInDecoder.Msg;
import cn.xyt.util.ByteUtil;

/**
 * 直接继承 ChannelOutboundHandlerAdapter 需要自己做 组包封装
 * @author tomsun
 */
@Deprecated
public class ByteOutEncoder1 extends ChannelOutboundHandlerAdapter {
	private static final Logger log = LoggerFactory.getLogger(ByteOutEncoder1.class);

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		Msg bb = (Msg) msg;
		ByteBuf buf = Unpooled.buffer().order(LITTLE_ENDIAN);
		buf.writeBytes(bb.content);
		ctx.write(buf);
	}
}
